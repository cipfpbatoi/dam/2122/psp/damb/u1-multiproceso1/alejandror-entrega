import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Ejercicio2_EjemploArturo {
    public static void main(String[] args) {

        String command = "java -jar ../Random10/out/artifacts/Random10_jar/Random10.jar";
        List<String> argsList = new ArrayList<>(List.of(command));
        ProcessBuilder pb = new ProcessBuilder(argsList);
        try {
            Process random10 = pb.start();
            Scanner random10Scanner = new Scanner(random10.getInputStream());

            //Envío de palabras al proceso
            OutputStream os = random10.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();

            while (!linea.equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(random10Scanner.nextLine());
                /**
                 * Falta leer la línea de la entrada
                 */
            }

        } catch (IOException ex) {
            System.out.println("Ha habido un error: IOException.");
            System.exit(1);
        }
    }
}