import java.io.*;

public class Ramdom10 {
    /**
     * No hacia falta complicarse tanto, además
     * has utilizado una sintaxis que no sé si la veis en 
     * el módulo
     */
    public static void main(String[] args) throws IOException {

        try (var stream = new InputStreamReader(System.in);
             var bffReader = new BufferedReader(stream)) {
            String line = "";
            do {
                line = bffReader.readLine();

                /**
                 * Aunque sólo haya una línea en la condición,
                 * es mejor poner siempre {}
                 */
                if(!line.equals("stop")) { 
                    System.out.println(Math.floor(Math.random()*10));
                }  

            } while (!line.equals("stop"));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }
    }
}
