import java.io.FileWriter;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Ejercicio1 {
    public static void main(String[] args) throws IOException {

        if(args.length <=0){
            System.err.println("I need a command to execute!!");
            System.exit(-1);
        }

        ProcessBuilder pb = new ProcessBuilder(args);

        try{
            Process process = pb.start();

            if(process.waitFor(2, TimeUnit.SECONDS)==true){

                int resultado = process.exitValue();

                System.out.println("Execution of "+ Arrays.toString(args)+" return exit value: "+resultado);

                InputStream is;

                if ( resultado == 0 ){
                    is = process.getInputStream();
                }else{
                    is = process.getErrorStream();
                }

                Scanner sc =  new Scanner(is);

                FileWriter fichero = null;
                PrintWriter pw = null;

                try{
                    fichero = new FileWriter("output.txt");
                    pw = new PrintWriter(fichero);

                    while ( sc.hasNext()){
                        if ( resultado == 0 ){
                            pw.println(sc.nextLine());
                        }else{
                            System.err.println(sc.nextLine());
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally{
                    try{
                        if(null !=fichero){
                            fichero.close();
                        }
                    }catch (Exception e2){
                        e2.printStackTrace();
                    }
                }

            }else{
                System.out.println("Se ha acabado el tiempo de espera...");
            }
        }catch (IOException ex){
            System.err.println("IO Exception !!");
            System.exit(-1);
        }catch (InterruptedException ex) {
            System.err.println("The child process exited with error");
            System.exit(-1);
        }
    }
}