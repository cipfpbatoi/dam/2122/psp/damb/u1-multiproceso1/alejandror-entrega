import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;

public class Minusculas {
    public static void main(String[] args) {
        try(var stream = new InputStreamReader(System.in);
            var bffReader = new BufferedReader(stream))
        {
            String line = "";
            do{
                line = bffReader.readLine();
                if(!line.equals("finalizar")) {
                    System.out.println(line.toLowerCase(Locale.ROOT));
                }
            }while (!line.equals("finalizar"));

        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }
    }
}
