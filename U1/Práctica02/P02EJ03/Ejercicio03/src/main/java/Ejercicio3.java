import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejercicio3 {
    private final static String finalizar = "finalizar";

    public static void main(String[] args)throws IOException,InterruptedException {
        String command ="java -jar /home/batoi/Escritorio/2 DAM/Programación de servicios y procesos/psp/U1/Práctica02/P02EJ03/Minusculas/out/artifacts/Minusculas_jar/Minusculas.jar";

        ProcessBuilder proccesTransf = new ProcessBuilder(command.split(" "));

        try{
            Process transf = proccesTransf.start();

            OutputStream os = transf.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner transfSc = new Scanner(transf.getInputStream());
            Scanner sc = new Scanner(System.in);

            String line =sc.nextLine();
            do{
                bw.write(line);
                bw.newLine();
                bw.flush();
                System.out.println(transfSc.nextLine());

                line = sc.nextLine();
            }while (!line.equals("finalizar"));
        }catch (IOException ex){
            Logger.getLogger(Ejercicio3.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
}
